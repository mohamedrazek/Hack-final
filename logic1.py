

from concurrent.futures import thread
from os import device_encoding
from this import s
import requests
import json
from requests.auth import HTTPBasicAuth
from urllib3 import disable_warnings
from pprint import pprint
from genie.conf import Genie
from genie.testbed import load
import yaml
from netmiko.ssh_autodetect import SSHDetect
from netmiko.ssh_dispatcher import ConnectHandler
import threading
import subprocess
from multiprocessing import Pool
import time
import os
import logging
import networkx as nx
import matplotlib.pyplot as plt


    
   
        
def get_interfaces_all(file:str):
    
    Interface_list=[]
    
    testbed=load(f"./temp/{file}")
    tb = Genie.init(testbed)
    for device in tb.devices.values():
        print(f'Collecting Interfaces from {device.name}')
        
        try: 
            device.connect(log_stdout=False)
            out= device.learn('interface').info
            # print(json.dumps(out,indent=4))
            for k,v in out.items():
                interface={}
                interface['name']=k
                interface['type']=v['type']
                interface['status']=v['oper_status']
                interface['mac']=v['phys_address']
                interface['enabled']=v['enabled']
                interface['in-pckts']=v['counters']['in_pkts']
                interface['out-pckts']=v['counters']['out_pkts']
                interface['in-errors']=v['counters']['in_errors']
                interface['out-errors']=v['counters']['out_errors']
                
                if "ipv4" in v.keys():
                    for a,b in v['ipv4'].items():
                        
                        interface['ip']=b['ip']
                        interface['prefix']=b['prefix_length']
                Interface_list.append(interface)
                
        

        except Exception as e:
            continue
        return device.name,Interface_list
    
def get_ospf_neighbors(file:str):
       
        ospf_nei={}
        ospf_routes={}
        testbed=load(f"./temp/{file}")
        tb = Genie.init(testbed)
        for device in tb.devices.values():
            print(f'Collecting OSPF from {device.name}')
            ospf_neighbors=[]
            try: 
                device.connect(log_stdout=False)
                out= device.learn('ospf').info
                out2= device.parse('show ip route')
        
                pa=out2['vrf']['default']['address_family']['ipv4']['routes']
                p=out['vrf']['default']['address_family']['ipv4']['instance']['1']['areas']['0.0.0.0']['interfaces']
                
                for k, v in p.items():
                    if "neighbors" in v.keys():
                        for nei,data in v['neighbors'].items():
                            #print(f"{device.name} has OSPF neighbor with {data['neighbor_router_id']} on {k}  {nei}")
                            ospf_nei[nei]=k
                
                for ka, va in pa.items():

                    if va["source_protocol"] == 'ospf':
                        

                        tmp={}
                        for a,b in va['next_hop']['next_hop_list'].items():

                            tmp[b['next_hop']]=b['outgoing_interface']
                        ospf_routes[ka]=tmp
                    else:
                        continue
            
            except:
                print('Null')
            return device.name,ospf_nei,ospf_routes
                

def get_lldp_all(file:str):
        
        lldp={}
        testbed=load(f"./temp/{file}")
        tb = Genie.init(testbed)
        for device in tb.devices.values():
            print(f'Collecting LLDP From {device.name}')
            
            try: 
                device.connect(log_stdout=False)
                out= device.learn('lldp').info
                
                # print(json.dumps(out,indent=4))
                for k,v in out['interfaces'].items():
                    if "neighbors" in v['port_id'][k].keys():
                        
                        lldp['connected-via']=k
                        for a,b in v['port_id'][k]['neighbors'].items():
                            lldp['name']=str(a).rsplit('.')[0]
                            lldp['neighbor-ip']=b['management_address']
                        
                    else:
                        continue
                        

            except Exception as e:
                logging.info(msg=e)
            
            return  device.name,lldp      

    


    
    
def host_details(IP:str):
    device ={
        'device_type': 'autodetect',
        'host':IP,
        'username':'admin',
        'password':'cisco'
    }
    guess=SSHDetect(**device)
    
    ios_c=guess.autodetect()
    
    host=ConnectHandler(
        device_type=ios_c,
        host=IP,
        username='admin',
        password='cisco'
    )
    if ios_c =='cisco_ios':
        ios='ios'
    else:
        ios="nxos"

    host_name=host.find_prompt().replace('#','')
    host.disconnect()
    print(f"{host_name}: {ios}")
    return IP,host_name,ios

def prep_inventory(devices):
    for i, device in enumerate(devices):
        f=open('./inventory.yml','r')
        doc=yaml.safe_load(f)
        doc['devices']['CE1']['connections']['default']['ip']=device[0]
        doc['devices']['CE1']['os']=device[2]
        doc['devices'][device[1]]=doc['devices'].pop('CE1')
        print(f"preparing file for {device[1]}")
        fc=open(f'./temp/inventory{i+1}.yml','w')
        yaml.safe_dump(doc,fc)
    
    
        


    

    











if __name__=="__main__":

    # s=time.time()
    devices=['192.168.0.23','192.168.0.233','192.168.0.189','192.168.0.195',"192.168.0.148"]
    pool=Pool(processes=5)
    result=pool.map(host_details,devices)
    prep_inventory(result)
    files=[]
    tmp_interface={}
    ospf_link=[]
    start=time.time()
    fd=os.scandir('./temp')
    for i in fd:
        files.append(i.name)
        
    pool1=Pool(processes=5)
    result1=pool1.map(get_interfaces_all,files)
    pool2=Pool(processes=5)
    result2=pool2.map(get_ospf_neighbors,files)
    # pool3=Pool(processes=5)
    # result3=pool3.map(get_lldp_all,files)
    
     
    for r in result1:
        # print(r[0])
        # print(json.dumps(r[1],indent=4))
        tmp=[]
        for i in r[1]:
            if "ip" in i.keys():
                tmp.append(i['ip'])
        tmp_interface[r[0]]=tmp
    
    for r in result2:
        # print(r[0])
        # print(json.dumps(r[1],indent=4))
        # print(json.dumps(r[2],indent=4))
        for ip in r[1].keys():
            for k,v in tmp_interface.items():
                for s in v:
                    if ip==s:
                        link={'SourceNode': r[0],'DestNode':k}
                        if {'SourceNode':k,'DestNode':r[0]} not in ospf_link:
                                ospf_link.append(link)
    finish=time.time()
    print(finish-start)
    for i in ospf_link:
        print(i)
    # for r in result3:
    #     print(r[0])
    #     print(json.dumps(r[1],indent=4))
    
    G=nx.Graph()
    for key in tmp_interface.keys():
        G.add_node(key)

    for i in ospf_link:
        G.add_edge(i['SourceNode'],i['DestNode'])
    nx.draw(G,with_labels=True,node_size=3000)
    plt.show()
    

    
    
    



